Pillar Vending Machine Kata https://github.com/PillarTechnology/kata-vending-machine

Kata built with Gradle 4.8.1

To run test:
From the project root execute the command "gradle cleanTest test"

The build has been modified to display the results of each test whether it be PASS, FAIL, or SKIPPED

The VendingMachineOperation class can be built and run to interface with the VendingMachine class through the console.