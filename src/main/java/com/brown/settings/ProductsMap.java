package com.brown.settings;

import java.util.Map;

public class ProductsMap {
     public static final Map<String, String> MAP = Map.of(
            "cola", "1.00",
            "chips", "0.50",
            "candy", "0.65"
    );
}
