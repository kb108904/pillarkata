package com.brown.settings;

public class VendingStatusMap {
    public enum Status
    {
        WAITING, VENDING, INVALID_SELECTION, COMPLETE, SOLD_OUT
    }
}
