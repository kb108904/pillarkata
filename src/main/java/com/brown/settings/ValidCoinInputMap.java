package com.brown.settings;

import java.util.Map;

public class ValidCoinInputMap {
    public static final Map<String, String> MAP = Map.of(
            "dime", "0.10",
            "quarter", "0.25",
            "nickel", "0.05",
            "d", "0.10",
            "q", "0.25",
            "n", "0.05"
    );
}
