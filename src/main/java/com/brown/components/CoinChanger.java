package com.brown.components;

import com.brown.settings.ValidCoinInputMap;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.brown.components.CoinChangerConstants.*;

public class CoinChanger {
    private BigDecimal currentAmountTotal = BigDecimal.ZERO;
    private ArrayList<BigDecimal> coinsInserted = new ArrayList<>();
    private ArrayList<String> coinReturn = new ArrayList<>();
    private static Map<String, Integer> changeAvailable = new HashMap<>();

    CoinChanger(){
    changeAvailable.put(TWENTY_FIVE_CENT_DECIMAL, 10);
    changeAvailable.put(TEN_CENT_DECIMAL, 10);
    changeAvailable.put(FIVE_CENT_DECIMAL, 10);
    }

    CoinChanger(int quarters, int dimes, int nickles){
        changeAvailable.put(TWENTY_FIVE_CENT_DECIMAL, quarters);
        changeAvailable.put(TEN_CENT_DECIMAL, dimes);
        changeAvailable.put(FIVE_CENT_DECIMAL, nickles);
    }

    BigDecimal insertCoin(String amount) {
        if(!amount.isEmpty()) {
            String validAmount = stringToStringDecimal(amount);
            BigDecimal insertedAmount = new BigDecimal(validAmount);
            if (insertedAmount.equals(BigDecimal.ZERO) && validAmount.equals(INVALID_COIN_ZERO)) {
                addCoinToCoinReturn(amount, false);
            } else {
                coinsInserted.add(insertedAmount);
            }
            setCurrentAmountTotal(getCurrentAmountTotal().add(insertedAmount));
        }
        return getCurrentAmountTotal();
    }

    private void setCurrentAmountTotal(BigDecimal currentAmountTotal) {
        this.currentAmountTotal = currentAmountTotal;
    }

    BigDecimal getCurrentAmountTotal() {
        return currentAmountTotal;
    }

    void cacheCurrentAmount(BigDecimal selectionPrice) {
        setCurrentAmountTotal(BigDecimal.ZERO);
        BigDecimal coinsInsertedTotal = BigDecimal.ZERO;
        for (BigDecimal coin: coinsInserted) {
            coinsInsertedTotal = coinsInsertedTotal.add(coin);
            int newCoinTotal = (changeAvailable.get(coin.toString()))+1;
            changeAvailable.put(coin.toString(), newCoinTotal);
        }
        coinsInserted.clear();
        if(coinsInsertedTotal.compareTo(selectionPrice) > 0){
            addCoinToCoinReturn(coinsInsertedTotal.subtract(selectionPrice).toString(), true);
        }
    }

    boolean changeAvailable() {
        return changeAvailable.get(TWENTY_FIVE_CENT_DECIMAL)>=10 && changeAvailable.get(TEN_CENT_DECIMAL)>=10 && changeAvailable.get(FIVE_CENT_DECIMAL)>=10;
    }

    String getCoinReturnTotal() {
        BigDecimal changeTotal = BigDecimal.ZERO;
        for(String coin: getCoinReturn()){
            changeTotal = changeTotal.add(new BigDecimal(coin));
        }
        if(changeTotal.compareTo(BigDecimal.ZERO)==0){
            return "";
        }else{
            return "$"+changeTotal.toString();
        }
    }

    private ArrayList<String> getCoinReturn() {
        return coinReturn;
    }

    private void addCoinToCoinReturn(String coins, boolean validCoins) {
        if(validCoins) {
            BigDecimal changeDue = new BigDecimal(coins);
            while (changeDue.compareTo(BigDecimal.ZERO) > 0) {
                if (changeDue.compareTo(new BigDecimal(TWENTY_FIVE_CENT_DECIMAL)) > -1) {
                    removeCoin(TWENTY_FIVE_CENT_DECIMAL);
                    coinReturn.add(TWENTY_FIVE_CENT_DECIMAL);
                    changeDue = changeDue.subtract(new BigDecimal(TWENTY_FIVE_CENT_DECIMAL));
                } else if (changeDue.compareTo(new BigDecimal(TEN_CENT_DECIMAL)) >-1) {
                    removeCoin(TEN_CENT_DECIMAL);
                    coinReturn.add(TEN_CENT_DECIMAL);
                    changeDue = changeDue.subtract(new BigDecimal(TEN_CENT_DECIMAL));
                } else if (changeDue.compareTo(new BigDecimal(FIVE_CENT_DECIMAL)) == 0) {
                    removeCoin(FIVE_CENT_DECIMAL);
                    coinReturn.add(FIVE_CENT_DECIMAL);
                    changeDue = changeDue.subtract(new BigDecimal(FIVE_CENT_DECIMAL));
                }
            }
        }else{
            coinReturn.add(coins);
        }
    }

    private void removeCoin(String coin) {
        int newCoinTotal = (changeAvailable.get(coin))-1;
        changeAvailable.put(coin, newCoinTotal);
    }

    /**
     * This function will take a string input compares that value to
     * known valid inputs then output the equivalent string value. Inputs
     * not equal to a valid input, but are valid numbers, will output a
     * INVALID_COIN_ZERO. Inputs that are not valid numbers will output INVALID_CURRENCY_ZERO
     * ie "quarter" => "0.25" or ".10" => "0.10" or "tenCents" => "0.00" or ".012" => "0"
     * @param amount valid coin string value
     * @return A valid string value equivalent to input string, or an equivalent ZERO value
     */
    private String stringToStringDecimal(String amount) {
        try {
            String amountLowerCase = amount.toLowerCase();
            if(ValidCoinInputMap.MAP.containsKey(amountLowerCase)){
                return ValidCoinInputMap.MAP.get(amountLowerCase);
            }else if(Double.valueOf(amountLowerCase).equals(.05) || Double.valueOf(amountLowerCase).equals(.1) || Double.valueOf(amountLowerCase).equals(.25)){
                return Double.valueOf(amountLowerCase).toString();
            }else{
                return INVALID_COIN_ZERO;
            }
        }catch (NumberFormatException e){
            return INVALID_CURRENCY_ZERO;
        }
    }

    @Override
    public String toString(){
        String output ="";
        for (Object o : changeAvailable.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            output = output + "$" + pair.getKey() + " : " + pair.getValue() + "\n";
        }
        return output.trim();
    }
}
