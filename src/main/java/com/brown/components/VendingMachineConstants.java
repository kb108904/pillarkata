package com.brown.components;

public final class VendingMachineConstants {
    public static final String INSERT_COIN_MSG = "INSERT COIN";
    public static final String EXACT_CHANGE_ONLY_MSG = "EXACT CHANGE ONLY";
    public static final String PREFIX_MSG = "$";
    public static final String THANK_YOU_MSG = "THANK YOU";
    public static final String PRICE_PREFIX_MSG = "PRICE $";
    public static final String SOLD_OUT_MSG = "SOLD OUT";
    public static final String VENDING_ERROR_MSG = "_ERROR_";
    public static final String CHIPS_ITEM = "CHIPS";
    public static final String COLA_ITEM = "COLA";
    public static final String CANDY_ITEM = "CANDY";
    public static final String COIN_RETURN_DISPLAY = "COIN RETURN";
}
