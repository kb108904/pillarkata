package com.brown.components;

public final class CoinChangerConstants {
    public static final String TWENTY_FIVE_CENT_DECIMAL = "0.25";
    public static final String TEN_CENT_DECIMAL = "0.10";
    public static final String FIVE_CENT_DECIMAL = "0.05";
    public static final String INVALID_COIN_ZERO = "0";
    public static final String INVALID_CURRENCY_ZERO = "0.00";
}
