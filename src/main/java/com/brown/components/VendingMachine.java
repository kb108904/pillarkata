package com.brown.components;

import com.brown.settings.ProductsMap;
import com.brown.settings.VendingStatusMap;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.brown.components.VendingMachineConstants.*;
import static com.brown.settings.VendingStatusMap.*;

public class VendingMachine {


    private static CoinChanger coinChanger;
    private BigDecimal lastSelectionAmount = BigDecimal.ZERO;
    private Status status = Status.WAITING;
    private static Map<String, Integer> stock = new HashMap<>();

    public VendingMachine() {
        stock.put(COLA_ITEM.toLowerCase(), 1);
        stock.put(CHIPS_ITEM.toLowerCase(), 1);
        stock.put(CANDY_ITEM.toLowerCase(), 1);
        coinChanger = new CoinChanger();
    }
    public VendingMachine(int cola, int chips, int candy, int quarters, int dimes, int nickles) {
        stock.put(COLA_ITEM.toLowerCase(), cola);
        stock.put(CHIPS_ITEM.toLowerCase(), chips);
        stock.put(CANDY_ITEM.toLowerCase(), candy);
        coinChanger = new CoinChanger(quarters, dimes, nickles);
    }

    public void insertCoin(String amount) {
        BigDecimal totalInserted = coinChanger.insertCoin(amount);
        if ((status.equals(Status.WAITING)) && (totalInserted.compareTo(BigDecimal.ZERO) > 0)) {
            setStatus(VendingStatusMap.Status.VENDING);
        }
    }

    public String getDisplay() {
        if (getStatus().equals(Status.WAITING)) {
            if (!coinChanger.changeAvailable()) {
                return EXACT_CHANGE_ONLY_MSG;
            }
            return INSERT_COIN_MSG;
        } else if (getStatus().equals(Status.VENDING)) {
            return PREFIX_MSG + coinChanger.getCurrentAmountTotal().toString();
        } else if (getStatus().equals(Status.COMPLETE)) {
            setStatus(Status.WAITING);
            return THANK_YOU_MSG;
        } else if (getStatus().equals(Status.INVALID_SELECTION)) {
            if (coinChanger.getCurrentAmountTotal().compareTo(BigDecimal.ZERO) > 0) {
                setStatus(Status.VENDING);
            } else {
                setStatus(Status.WAITING);
            }
            return PRICE_PREFIX_MSG + getLastSelectionAmount().toString();
        } else if (getStatus().equals(Status.SOLD_OUT)) {
            setStatus(Status.VENDING);
            return SOLD_OUT_MSG;
        } else {
            return VENDING_ERROR_MSG;
        }
    }

    public void selectProduct(String selection) {
        String selectionLowercase = selection.toLowerCase();
        BigDecimal selectionPrice = new BigDecimal(ProductsMap.MAP.get(selectionLowercase));
        if ((coinChanger.getCurrentAmountTotal().compareTo(selectionPrice) == 0) && isInStock(selectionLowercase)) {
            coinChanger.cacheCurrentAmount(selectionPrice);
            dispenseProduct(selectionLowercase);
            setStatus(Status.COMPLETE);
        } else if ((coinChanger.getCurrentAmountTotal().compareTo(selectionPrice) > 0) && isInStock(selectionLowercase)) {
            coinChanger.cacheCurrentAmount(selectionPrice);
            dispenseProduct(selectionLowercase);
            setStatus(Status.COMPLETE);
        } else if (coinChanger.getCurrentAmountTotal().compareTo(selectionPrice) < 0) {
            setLastSelectionAmount(selectionPrice);
            setStatus(Status.INVALID_SELECTION);
        } else if (!isInStock(selectionLowercase)) {
            setStatus(Status.SOLD_OUT);
        }
    }

    private BigDecimal getLastSelectionAmount() {
        return lastSelectionAmount;
    }

    private void setLastSelectionAmount(BigDecimal lastSelectionAmount) {
        this.lastSelectionAmount = lastSelectionAmount;
    }

    private Status getStatus() {
        return status;
    }

    private void setStatus(Status status) {
        this.status = status;
    }

    private boolean isInStock(String selection) {
        String selectionLowerCase = selection.toLowerCase();
        return stock.get(selectionLowerCase) > 0;
    }

    private int getItemStock(String selection) {
        String selectionLowerCase = selection.toLowerCase();
        return stock.get(selectionLowerCase);
    }

    private void dispenseProduct(String selection) {
        String selectionLowerCase = selection.toLowerCase();
        int stockLeft = (stock.get(selectionLowerCase)) - 1;
        stock.put(selectionLowerCase, stockLeft);
    }

    public BigDecimal getCurrentAmountTotal() {
        return coinChanger.getCurrentAmountTotal();
    }

    public String getCoinReturn() {
        return coinChanger.getCoinReturnTotal();
    }

    @Override
    public String toString() {
        return coinChanger.toString();
    }

    public String getVendingMachineDisplayFront() {
        return ":{"+CHIPS_ITEM+"}-"+getItemStock(CHIPS_ITEM)+" :{"+CANDY_ITEM+"}-"+getItemStock(CANDY_ITEM)+" :{"+COLA_ITEM+"}-"+getItemStock(COLA_ITEM)+"\n" +
                getDisplay()+"\n" +
                COIN_RETURN_DISPLAY+":"+getCoinReturn();
    }

    public void read(String input) {
        if(!isSelection(input)){
            insertCoin(input);
        }else if(isSelection(input)){
            selectProduct(input);
        }
    }

    private boolean isSelection(String input) {
        return stock.containsKey(input.toLowerCase());
    }
}
