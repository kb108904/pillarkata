package com.brown.main;

import com.brown.components.VendingMachine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VendingMachineOperation {
    private static VendingMachine VM = new VendingMachine();

    public static void main(String[] args) throws IOException
    {
        VM = new VendingMachine(1,1,1,9,9,9);
        String input = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(!input.equals("-1")) {
            System.out.println("~~~~~~|-----------------------|~~~~~");
            System.out.println("~~~~~~|  THE VENDING MACHINE  |~~~~~~");
            System.out.println("~~~~~~|_______________________|~~~~~~");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println(VM.getVendingMachineDisplayFront());
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("~~~~~~~~~~_________________~~~~~~~~~~");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.print("->");
            input = reader.readLine();
            VM.read(input);
        }
    }
}