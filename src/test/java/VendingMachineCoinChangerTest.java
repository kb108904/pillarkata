import com.brown.components.VendingMachine;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertEquals;

public class VendingMachineCoinChangerTest {

    VendingMachine vendingMachine;

    @Before
    public void setup(){
        vendingMachine = new VendingMachine();
    }

    @Test
    public void whenQuarterIsInsertedIntoVMCoinChangerCurrentAmountEqualsAQuarter(){
        vendingMachine.insertCoin("quarter");
        assertEquals(new BigDecimal("0.25"), vendingMachine.getCurrentAmountTotal());
        assertEquals("$0.25", vendingMachine.getDisplay());
    }

    @Test
    public void whenDimeIsInsertedIntoVMCoinChangerCurrentAmountEqualsADime(){
        vendingMachine.insertCoin("dime");
        assertEquals(new BigDecimal("0.10"), vendingMachine.getCurrentAmountTotal());
        assertEquals("$0.10", vendingMachine.getDisplay());
    }

    @Test
    public void whenInvalidCoinIsInsertedIntoVMCoinChangerCurrentAmountEqualsZero(){
        vendingMachine.insertCoin(".12");
        assertEquals(BigDecimal.ZERO, vendingMachine.getCurrentAmountTotal());
        assertEquals("$0.12", vendingMachine.getCoinReturn());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
    }

    @Test
    public void whenInvalidCoinIsInsertedIntoVMCoinChangerCurrentAmountEqualsZeroAndShownInCoinReturn(){
        vendingMachine.insertCoin(".01");
        assertEquals(BigDecimal.ZERO, vendingMachine.getCurrentAmountTotal());
        assertEquals("$0.01", vendingMachine.getCoinReturn());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
    }

    @Test
    public void whenNoCoinIsInsertedIntoVMCoinChangerTheDisplayWillReadInsertCoin(){
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
    }

    @Test
    public void whenValidCoinIsInsertedIntoVMCoinChangerTheDisplayWillReadCurrentAmount(){
        vendingMachine.insertCoin("dime");
        assertEquals("$0.10", vendingMachine.getDisplay());
    }

    @Test
    public void whenTwoQuartersAreInsertedIntoVMCoinChangerCurrentAmountAndDisplayAreUpdated(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        assertEquals(new BigDecimal("0.50"), vendingMachine.getCurrentAmountTotal());
        assertEquals("$0.50", vendingMachine.getDisplay());
    }

    @Test
    public void whenChangeIsMadeForUserTheAmountIsTracked(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("candy");
        assertEquals("$2.35", vendingMachine.getCoinReturn());
        assertEquals("$0.05 : 10\n$0.25 : 13\n$0.10 : 9", vendingMachine.toString());
    }
}
