import com.brown.components.VendingMachine;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class VendingMachineExactChangeModeTest {

    VendingMachine vendingMachine;

    @Before
    public void setup(){
        vendingMachine = new VendingMachine();
    }

    @Test
    public void whenThereIsNotEnoughChangeAvailableDisplayUpdate(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("candy");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("EXACT CHANGE ONLY", vendingMachine.getDisplay());
    }

    @Test
    public void whenThereIsEnoughChangeAvailableDisplayUpdate(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("chips");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
    }
}
