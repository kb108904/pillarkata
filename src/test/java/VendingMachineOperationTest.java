import com.brown.components.VendingMachine;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class VendingMachineOperationTest {

    VendingMachine vendingMachine;

    @Before
    public void setup(){
        vendingMachine = new VendingMachine();
    }

    @Test
    public void whenVendingMachineFrontIsCalledItShowsTheStartingDefaultState(){
        assertEquals(":{CHIPS}-1 :{CANDY}-1 :{COLA}-1\n" + "INSERT COIN\n" + "COIN RETURN:", vendingMachine.getVendingMachineDisplayFront());
    }

    @Test
    public void whenVendingMachineFrontIsCalledAndMoneyIsAddedItShowsTheAmount(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        assertEquals(":{CHIPS}-1 :{CANDY}-1 :{COLA}-1\n" + "$1.00\n" + "COIN RETURN:", vendingMachine.getVendingMachineDisplayFront());
    }

    @Test
    public void whenVendingMachineFrontIsCalledTheStockOfTheProductIsCorrect(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        assertEquals(":{CHIPS}-1 :{CANDY}-1 :{COLA}-0\n" + "THANK YOU\n" + "COIN RETURN:", vendingMachine.getVendingMachineDisplayFront());
    }

    @Test
    public void whenVendingMachineFrontIsCalledTheReturnedChangeIsCorrect(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        assertEquals(":{CHIPS}-1 :{CANDY}-1 :{COLA}-0\n" + "THANK YOU\n" + "COIN RETURN:$2.00", vendingMachine.getVendingMachineDisplayFront());
    }

    @Test
    public void whenVendingMachineIsGivenMoneyInputItWillUpdateAccordingly(){
        vendingMachine.read("dime");
        assertEquals(":{CHIPS}-1 :{CANDY}-1 :{COLA}-1\n" + "$0.10\n" + "COIN RETURN:", vendingMachine.getVendingMachineDisplayFront());
    }

    @Test
    public void whenVendingMachineIsGivenASelectionInputItWillUpdateAccordingly(){
        vendingMachine.read("cola");
        assertEquals(":{CHIPS}-1 :{CANDY}-1 :{COLA}-1\n" + "PRICE $1.00\n" + "COIN RETURN:", vendingMachine.getVendingMachineDisplayFront());
    }

    @Test
    public void whenVendingMachineIsGivenBadInputItWillEatIt(){
        vendingMachine.read("coola");
        assertEquals(":{CHIPS}-1 :{CANDY}-1 :{COLA}-1\n" + "INSERT COIN\n" + "COIN RETURN:", vendingMachine.getVendingMachineDisplayFront());
    }
}
