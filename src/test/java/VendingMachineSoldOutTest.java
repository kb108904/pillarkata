import com.brown.components.VendingMachine;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class VendingMachineSoldOutTest {

    VendingMachine vendingMachine;

    @Before
    public void setup(){
        vendingMachine = new VendingMachine();
    }

    @Test
    public void whenAProductIsSelectedAndThereIsMoreThanEnoughMoneyChangeIsReturned(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        assertEquals("SOLD OUT", vendingMachine.getDisplay());
        assertEquals("$1.00", vendingMachine.getDisplay());
    }
}
