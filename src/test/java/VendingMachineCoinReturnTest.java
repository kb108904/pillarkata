import com.brown.components.VendingMachine;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class VendingMachineCoinReturnTest {

    VendingMachine vendingMachine;

    @Before
    public void setup(){
        vendingMachine = new VendingMachine();
    }

    @Test
    public void whenAProductIsSelectedAndThereIsMoreThanEnoughMoneyChangeIsReturned(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("dime");
        vendingMachine.selectProduct("cola");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("$0.10", vendingMachine.getCoinReturn());
        assertEquals("$0.05 : 10\n$0.25 : 14\n$0.10 : 10", vendingMachine.toString());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
    }

    @Test
    public void whenNothingIsInsertedAndProductIsSelectedCorrectChangeIsProduced(){
        vendingMachine.insertCoin("");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("", vendingMachine.getCoinReturn());
        assertEquals("$0.05 : 10\n$0.25 : 14\n$0.10 : 10", vendingMachine.toString());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
    }

    @Test
    public void whenExactChangeIsUsedAndMultipleItemsSelectedCorrectChangeIsProduced(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("", vendingMachine.getCoinReturn());
        assertEquals("$0.05 : 10\n$0.25 : 14\n$0.10 : 10", vendingMachine.toString());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("nickel");
        vendingMachine.insertCoin("dime");
        vendingMachine.selectProduct("candy");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("", vendingMachine.getCoinReturn());
        assertEquals("$0.05 : 11\n$0.25 : 16\n$0.10 : 11", vendingMachine.toString());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
    }
}
