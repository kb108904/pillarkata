import com.brown.components.VendingMachine;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertEquals;

public class VendingMachineProductSelectionTest {


    VendingMachine vendingMachine;

    @Before
    public void setup(){
        vendingMachine = new VendingMachine();
    }

    @Test
    public void whenAProductIsSelectedAndThereIsEnoughMoneyProductIsDispensedAndDisplayIsUpdated(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
        assertEquals(BigDecimal.ZERO, vendingMachine.getCurrentAmountTotal());
    }

    @Test
    public void whenAProductIsSelectedAndThereIsNotEnoughMoneyDisplayIsUpdated(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("cola");
        assertEquals("PRICE $1.00", vendingMachine.getDisplay());
        assertEquals("$0.75", vendingMachine.getDisplay());
        assertEquals(new BigDecimal("0.75"), vendingMachine.getCurrentAmountTotal());
    }

    @Test
    public void whenAProductIsSelectedAndThereIsNoMoneyDisplayIsUpdated(){
        vendingMachine.selectProduct("cola");
        assertEquals("PRICE $1.00", vendingMachine.getDisplay());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
        assertEquals(BigDecimal.ZERO, vendingMachine.getCurrentAmountTotal());
    }

    @Test
    public void whenAProductIsSelectedWithCapitalizationAndThereIsEnoughMoneyProductIsDispensedAndDisplayIsUpdated(){
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.insertCoin("quarter");
        vendingMachine.selectProduct("Cola");
        assertEquals("THANK YOU", vendingMachine.getDisplay());
        assertEquals("INSERT COIN", vendingMachine.getDisplay());
        assertEquals(BigDecimal.ZERO, vendingMachine.getCurrentAmountTotal());
    }
}
